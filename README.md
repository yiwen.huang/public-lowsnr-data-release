# Characterization of low-significance gravitational-wave compact binary sources

Data release (PSD, prior distribution and posterior distribution) for : [1810.10035](https://arxiv.org/abs/1810.10035).
You can cite this repository by: 
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1601151.svg)](https://doi.org/10.5281/zenodo.1601151)

Note: to clone the hdf5 managed by git-lfs correctly, you will need to either clone it with git-lfs on your local machine or download the file directly here.


## data structure 

PSD: ['PSD'] [gpsTimes] [IFOs] ['frequency' or 'power_spectrum']

Prior: ['priors'] [CBCs]

Posterior : ['posteriors'] [CBCs] [INCs] [gpsTimes] [SNRs]


For your convenience:
```python
gpsTimes=['1135924088','1135989351','1136267078','1136506663','1136594611']
IFOs=['H','L','V']
CBCs=['BNS','NSBH','BBH','hBBH']
INCs=['30','80']
SNRs=['6','7','8','9','10','12']
```




## Examples

### Read the hdf5
```python
import h5py
# in your git folder
sample_file='public-lowsnr-data-release/lowsnr-samples.hdf5'
data=h5py.File(sample_file,'r')
```

### Print available parameters
Luminosity distance (Mpc): ```distance```

Right ascension (rad): ```ra```

Declination (rad): ```dec```

Effective spin (dimensionless): ```chi_eff```

Effective precessing spin (dimensionless): ```chi_p```

Chirp mass (Msun): ```chirp_mass```

Mass ratio (dimensionless): ```q```

Mass 1 (Msun): ```m1```

Mass 2 (Msun): ```m2```

Phase (rad): ```phase```

Inclination (rad): ```inclination```

Polarization (rad): ```polarization```

Arrival time (sec): ```time```

```python
#print all available parameters
print data['priors']['BBH'].items()
```


### Power Spectrum Density (PSD) 
For example: to get PSD for Virgo, for 1135924088
```python
from matplotlib import pylab as pl
%pylab inline

psd=pl.figure(1,figsize=(4,3),dpi=200)
ax=pl.subplot(111)
ax.set_xlim([10,1000]) # set the relative range

freq=data['PSD']['1135924088']['V']['frequency']
spectrum=data['PSD']['1135924088']['V']['power_spectrum']

loglog(freq,spectrum)
ylabel('PSD (Hz$^{-1}$)',fontsize=15)
xlabel('Frequency (Hz)',fontsize=15)
legend()
```
You should be able to get from the above code:

![virgo PSD](psd-test-virgo.png)


### Prior
To get prior distribution (only different for CBC type)
```python
prior_data=data['priors']['BBH']
mc_prior=prior_data['chirp_mass']

#plotting the prior distribution
mc_prior_plot=pl.figure(1,figsize=(4,3),dpi=200)
ax=pl.subplot(111)
ax.hist(mc_prior,bins=15)
ylabel('Probability',fontsize=15)
xlabel(r'$\mathcal{M} (M_{\odot})$',fontsize=15)
legend()

```

You should be able to get from the above code:

![prior distribution plot for chirp mass](chirp-mass-prior.png)

### Posterior
To get a posterior samples from posterior (CBC, inclination, GPS Time, SNR, posterior distribution or the injected value)
```python
pos_data=data['posteriors']['BBH']['80']['1135924088']['12']
#print all available parameters (and their data type and shape)
print pos_data.items()
# posterior distribution
mc_pos=pos_data['chirp_mass']
# injected value
mc_inj=pos_data['chirp_mass'].attrs['injected_value'] # it is a string
# find units
mc_units=pos_data['chirp_mass'].attrs['units']

# plotting the posterior distribution
mc_pos_plot=pl.figure(1,figsize=(4,3),dpi=200)
ax=pl.subplot(111)
ax.hist(mc_pos,bins=15)
ax.axvline(x=float(mc_inj), color='r', linestyle='-',label='injected') # plot the injected value
ylabel('Probability',fontsize=15)
xlabel(r'$\mathcal{M} (M_{\odot})$',fontsize=15)
legend()

```
You should be able to get from the above code:

![posterior distribution plot for chirp mass](chirp-mass-posterior.png)

*Note: ```phase``` does not have injected value, so 'injected_value' will return None
